package com.allrites.lib.api.retrofit_services

import com.allrites.lib.api.api_activity.ApiActivityResponse
import com.allrites.lib.api.video.VideoResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ContentRetrofitService {

    @POST("api/api-activity")
    fun postApiActivity(
        @Query("token") token: String,
        @Query("company_id") companyId: String,
        @Query("device") device: String = "android"
    ): Single<ApiActivityResponse>

    @GET("api/video")
    fun getVideo(
        @Query("token") token: String,
        @Query("video_id") videoId: String
    ): Single<VideoResponse>

}