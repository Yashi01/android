package com.allrites.lib.api

import com.google.gson.annotations.SerializedName

private const val STATUS_SUCCESS = "success"

abstract class BaseApiResponse {

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

    open fun isSuccessful(): Boolean {
        return STATUS_SUCCESS == status
    }

}