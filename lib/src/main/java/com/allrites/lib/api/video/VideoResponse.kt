package com.allrites.lib.api.video

import com.allrites.lib.api.BaseApiResponse
import com.allrites.lib.core.VideoDetails
import com.google.gson.annotations.SerializedName

class VideoResponse : BaseApiResponse() {

    @SerializedName("data")
    var videoDetails: VideoDetails? = null

    fun getVideoData(): VideoDetails {
        return videoDetails!!
    }

    override fun isSuccessful(): Boolean {
        return super.isSuccessful() && videoDetails != null
    }

}