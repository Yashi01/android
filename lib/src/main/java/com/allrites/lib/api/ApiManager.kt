package com.allrites.lib.api

import com.allrites.lib.SessionHolder
import com.allrites.lib.api.api_activity.ApiActivityResponse
import com.allrites.lib.api.retrofit_services.ApiRetrofitService
import com.allrites.lib.api.retrofit_services.ContentRetrofitService
import com.allrites.lib.api.video.VideoResponse
import com.allrites.lib.api.views_video.ViewsVideoRequest
import com.allrites.lib.api.views_video.ViewsVideoResponse
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiManager @Inject constructor() {

    fun postApiActivity(): Single<ApiActivityResponse> {
        val clientId = sessionHolder.clientId
        val apiToken = sessionHolder.apiToken
        return contentRetrofitService.postApiActivity(apiToken, clientId)
    }

    fun getVideo(videoId: String): Single<VideoResponse> {
        val apiToken = sessionHolder.apiToken
        return contentRetrofitService.getVideo(apiToken, videoId)
    }

    fun postViewsVideo(viewsVideoRequest: ViewsVideoRequest): Single<ViewsVideoResponse> {
        return apiRetrofitService.postViewsVideo(sessionHolder.apiToken, viewsVideoRequest)
    }

    //region Dependencies
    @Inject
    lateinit var sessionHolder: SessionHolder

    @Inject
    lateinit var contentRetrofitService: ContentRetrofitService

    @Inject
    lateinit var apiRetrofitService: ApiRetrofitService
    //endregion

}