package com.allrites.lib.api.retrofit_services

import com.allrites.lib.api.views_video.ViewsVideoRequest
import com.allrites.lib.api.views_video.ViewsVideoResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiRetrofitService {

    @Headers(
        "Content-Type: application/json; charset=utf-8",
    )
    @POST("views/v1/video")
    fun postViewsVideo(
        @Header("Authorization") token: String,
        @Body body: ViewsVideoRequest
    ): Single<ViewsVideoResponse>

}