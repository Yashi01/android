package com.allrites.lib.api.views_video

import com.google.gson.annotations.SerializedName

class ViewsVideoRequest {

    @SerializedName("id")
    var id: Double = -1.0

    @SerializedName("video_id")
    var videoId: Long = -1L

    @SerializedName("title")
    var title: String = ""

    @SerializedName("duration")
    var duration: Long = -1L

    @SerializedName("video_series")
    var videoSeries: Boolean = false

    @SerializedName("video_episode")
    var videoEpisode: String? = null

    @SerializedName("company_id")
    var companyId: String = ""

    @SerializedName("device")
    val device: String = "android"

    @SerializedName("view_start")
    var viewStart: Long = -1

    @SerializedName("view_end")
    var viewEnd: Long = -1

    @SerializedName("timestamp")
    var timestamp: String? = null

    @SerializedName("token")
    var token: String = ""

    @SerializedName("ip_address")
    var ipAddress: String? = null

}