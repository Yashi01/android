package com.allrites.lib

import android.content.Context
import com.allrites.lib.core.ErrorNotifier
import com.allrites.lib.core.VideoDataHolder
import com.allrites.lib.core.VideoIdRetriever
import com.allrites.lib.di.DaggerRootObjectGraph
import com.allrites.lib.di.RetrofitDaggerModule
import com.allrites.lib.di.RootObjectGraph
import com.allrites.lib.di.SystemDaggerModule
import com.google.android.exoplayer2.Player
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject

class AllRitesSdk(context: Context) {

    private var rootObjectGraph: RootObjectGraph = DaggerRootObjectGraph
        .builder()
        .systemDaggerModule(SystemDaggerModule(context))
        .retrofitDaggerModule(RetrofitDaggerModule())
        .build()

    init {
        rootObjectGraph.inject(this)
    }

    private var initialRequestsSenderDisposable: Disposable? = null

    fun init(apiToken: String, clientId: String) {
        sessionHolder.apiToken = apiToken
        sessionHolder.clientId = clientId
    }

    fun deinit() {
        sessionHolder.clear()
        disposeInitialRequestsSenderDisposable()
        videoDataHolder.reset()
        errorNotifier.reset()
    }

    fun startVideoSession(player: Player) {
        if (videoIdRetriever.retrieveVideoId(player)) {
            doInitialRequestsLogic(player)
        }
    }

    private var initialRequestsWasSuccessful = false

    private fun doInitialRequestsLogic(player: Player) {
        initialRequestsSenderDisposable = initialRequestsSender.sendInitialRequests(videoDataHolder.videoId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                initialRequestsWasSuccessful = true
                playerHandler.init(player)
            }, {
                initialRequestsWasSuccessful = false
                errorNotifier.notifyError(it.message)
            })
    }

    fun endVideoSession() {
        if (initialRequestsWasSuccessful) {
            playerHandler.deInit()
        }
        disposeInitialRequestsSenderDisposable()
    }

    private fun disposeInitialRequestsSenderDisposable() {
        initialRequestsSenderDisposable?.apply {
            dispose()
            initialRequestsSenderDisposable = null
        }
    }

    fun setErrorCallback(callback: (String) -> Unit) {
        errorNotifier.setErrorCallback(callback)
    }

    //region Dependencies
    @Inject
    lateinit var sessionHolder: SessionHolder

    @Inject
    lateinit var playerHandler: PlayerHandler

    @Inject
    lateinit var initialRequestsSender: InitialRequestsSender

    @Inject
    lateinit var videoDataHolder: VideoDataHolder

    @Inject
    lateinit var errorNotifier: ErrorNotifier

    @Inject
    lateinit var videoIdRetriever: VideoIdRetriever
    //endregion

}
