package com.allrites.lib

import android.util.Log
import com.allrites.lib.api.ApiManager
import com.allrites.lib.api.views_video.ViewsVideoRequest
import com.allrites.lib.core.ErrorNotifier
import com.allrites.lib.core.VideoDataHolder
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.round

@Singleton
class PlaybackDataSender @Inject constructor() {

    /**
     * in milliseconds.
     */
    private var lastCurrentTime = 0L

    fun initEvent(currentTime: Long) {
        log("initEvent(): currentTime=$currentTime")
        sendViewsVideoRequest(lastCurrentTime, currentTime)
    }

    /**
     * [currentTime] in milliseconds.
     */
    fun playEvent(currentTime: Long) {
        log("playEvent(): currentTime=$currentTime")
        lastCurrentTime = currentTime
    }

    /**
     * [currentTime] in milliseconds.
     */
    fun pauseEvent(currentTime: Long) {
        log("pauseEvent(): currentTime=$currentTime")
        sendViewsVideoRequest(lastCurrentTime, currentTime)
    }

    /**
     * [currentTime] in milliseconds.
     */
    fun completedEvent(currentTime: Long) {
        log("completedEvent(): currentTime=$currentTime")
        sendViewsVideoRequest(lastCurrentTime, currentTime)
    }

    private var lastSentSeconds = 0L

    /**
     * [currentTime] in milliseconds.
     */
    fun progressChanged(currentTime: Long) {
        val seconds = round(currentTime / 1000.0).toLong()
        if (seconds != lastSentSeconds && seconds % 30 == 0L) {
            lastSentSeconds = seconds
            sendProgress(currentTime)
        }
    }

    private var postViewsVideoDisposable: Disposable? = null

    /**
     * [currentTime] in milliseconds.
     */
    private fun sendProgress(currentTime: Long) {
        log("sendProgress(): currentTime=$currentTime")
        sendViewsVideoRequest(0, currentTime)
    }

    /**
     * [viewStart] in milliseconds.
     * [viewEnd] in milliseconds.
     */
    private fun sendViewsVideoRequest(viewStart: Long, viewEnd: Long) {
        log("sendViewsVideoRequest(): viewStart=$viewStart, viewEnd=$viewEnd")
        val data = buildViewsVideoRequest(viewStart, viewEnd)
        postViewsVideoDisposable = apiManager.postViewsVideo(data)
            .subscribe({
            }, {
                errorNotifier.notifyError(it.message)
            })
    }

    /**
     * [viewStart] in milliseconds.
     * [viewEnd] in milliseconds.
     */
    private fun buildViewsVideoRequest(viewStart: Long, viewEnd: Long): ViewsVideoRequest {
        val videoData = videoDataHolder.getVideoData()
        val request = ViewsVideoRequest()
        with(request) {
            id = videoDataHolder.id
            videoId = videoData.id
            title = videoData.title
            duration = videoData.duration.toLong() * 60000
            videoSeries = videoData.isSeries
            videoEpisode = ""
            companyId = sessionHolder.clientId
            this.viewStart = viewStart
            this.viewEnd = viewEnd
            timestamp = null
            token = sessionHolder.apiToken
            ipAddress = null
        }
        return request
    }

    fun reset() {
        lastSentSeconds = 0L
        postViewsVideoDisposable?.apply {
            dispose()
            postViewsVideoDisposable = null
        }
        lastCurrentTime = 0L
    }

    private val logTag = PlaybackDataSender::class.simpleName!!

    private fun log(message: String) {
        Log.d(logTag, message)
    }

    //region Dependencies
    @Inject
    lateinit var apiManager: ApiManager

    @Inject
    lateinit var videoDataHolder: VideoDataHolder

    @Inject
    lateinit var errorNotifier: ErrorNotifier

    @Inject
    lateinit var sessionHolder: SessionHolder
    //endregion

}