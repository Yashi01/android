package com.allrites.lib.core

import android.text.TextUtils
import com.allrites.lib.Utils
import com.google.android.exoplayer2.Player
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VideoIdRetriever @Inject constructor() {

    fun retrieveVideoId(player: Player): Boolean {
        val uri = player.currentMediaItem?.playbackProperties?.uri
        if (uri == null) {
            notifyError("Failed to retrieve a playback url")
            return false
        }

        val videoId = Utils.retrieveVideoId(uri)
        return if (TextUtils.isEmpty(videoId)) {
            notifyError("Failed to retrieve videoId from url=$uri")
            false
        } else {
            videoDataHolder.videoId = videoId
            true
        }
    }

    private fun notifyError(message: String) {
        errorNotifier.notifyError(message)
    }

    //region Dependencies
    @Inject
    lateinit var errorNotifier: ErrorNotifier

    @Inject
    lateinit var videoDataHolder: VideoDataHolder
    //endregion

}