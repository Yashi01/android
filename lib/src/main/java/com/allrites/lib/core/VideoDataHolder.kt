package com.allrites.lib.core

import com.allrites.lib.Utils
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VideoDataHolder @Inject constructor() {

    private var videoDetails: VideoDetails? = null
    var id: Double = -1.0

    fun setVideoData(videoDetails: VideoDetails) {
        this.videoDetails = videoDetails
        id = Utils.generateVideoId()
    }

    fun getVideoData(): VideoDetails {
        return videoDetails!!
    }

    var videoId = ""

    fun reset() {
        videoDetails = null
        id = -1.0
        videoId = ""
    }
}