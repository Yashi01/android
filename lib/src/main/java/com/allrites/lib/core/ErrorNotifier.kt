package com.allrites.lib.core

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ErrorNotifier @Inject constructor() {

    private var callback: ((String) -> Unit)? = null

    fun setErrorCallback(callback: (String) -> Unit) {
        this.callback = callback
    }

    fun notifyError(error: String?) {
        val text = error ?: "Unknown error"
        callback?.invoke(text)
    }

    fun reset() {
        callback = null
    }

}