package com.allrites.lib.core

import com.google.gson.annotations.SerializedName

class VideoDetails {

    @SerializedName("id")
    var id: Long = -1

    @SerializedName("title")
    var title = ""

    @SerializedName("duration")
    var duration = ""

    @SerializedName("isSeries")
    var isSeries = false

}