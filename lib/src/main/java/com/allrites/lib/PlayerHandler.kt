package com.allrites.lib

import android.util.Log
import com.google.android.exoplayer2.Player
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlayerHandler @Inject constructor() {

    private var player: Player? = null
    private fun requirePlayer() = player!!

    fun init(player: Player) {
        this.player = player
        addPlayerListener()
        startPlaybackPositionLogic()
        initEvent()
    }

    fun deInit() {
        stopPlaybackPositionLogic()
        removePlayerListener()
        player = null
        playbackDataSender.reset()
    }

    private fun addPlayerListener() {
        requirePlayer().addListener(playerListener)
    }

    private fun removePlayerListener() {
        requirePlayer().removeListener(playerListener)
    }

    private val playerListener = object : Player.EventListener {

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            log("onPlayerStateChanged(): playWhenReady=$playWhenReady, playbackState=$playbackState")
            if (Player.STATE_ENDED == playbackState) {
                completedEvent()
            }
        }

        override fun onIsPlayingChanged(isPlaying: Boolean) {
            log("onIsPlayingChanged(): isPlaying=$isPlaying")
            if (isPlaying) {
                playEvent()
            } else {
                pauseEvent()
            }
        }
    }

    private fun initEvent() {
        playbackDataSender.initEvent(getCurrentPlayerPosition())
    }

    private fun playEvent() {
        playbackDataSender.playEvent(getCurrentPlayerPosition())
    }

    private fun pauseEvent() {
        playbackDataSender.pauseEvent(getCurrentPlayerPosition())
    }

    private fun completedEvent() {
        playbackDataSender.completedEvent(getCurrentPlayerPosition())
    }

    private var playbackPositionDisposable: Disposable? = null

    private fun startPlaybackPositionLogic() {
        playbackPositionDisposable =
            Observable.interval(500, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .map { _ ->
                    getCurrentPlayerPosition()
                }.subscribe { currentPosition ->
                    progressChanged(currentPosition)
                }
    }

    private fun getCurrentPlayerPosition(): Long {
        return requirePlayer().currentPosition
    }

    private fun stopPlaybackPositionLogic() {
        playbackPositionDisposable?.dispose()
    }

    /**
     * [currentTime] in millis.
     */
    private fun progressChanged(currentTime: Long) {
        playbackDataSender.progressChanged(currentTime)
    }

    private val logTag = PlayerHandler::class.simpleName!!

    private fun log(message: String) {
        Log.d(logTag, message)
    }

    //region Dependencies
    @Inject
    lateinit var playbackDataSender: PlaybackDataSender
    //endregion

}