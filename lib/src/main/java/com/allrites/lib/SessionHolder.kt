package com.allrites.lib

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SessionHolder @Inject constructor() {

    var apiToken: String = ""
    var clientId: String = ""

    fun clear() {
        apiToken = ""
        clientId = ""
    }
}