package com.allrites.lib

import android.net.Uri
import java.util.regex.Pattern
import kotlin.math.floor

class Utils private constructor() {

    companion object {

        fun generateVideoId(): Double {
            return floor(100000000 + Math.random() * 900000000)
        }

        private val pattern = Pattern.compile("/([0-9]{1,6})/")

        fun retrieveVideoId(uri: Uri): String {
            val url = uri.toString()
            val matcher = pattern.matcher(url)
            if (matcher.find()) {
                return matcher.group(1)!!
            }
            return ""
        }

    }
}