package com.allrites.lib.di

import com.allrites.lib.BuildConfig
import com.allrites.lib.api.retrofit_services.ApiRetrofitService
import com.allrites.lib.api.retrofit_services.ContentRetrofitService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

private const val CONTENT_RETROFIT = "content_retrofit"
private const val API_RETROFIT = "api_retrofit"

@Module
class RetrofitDaggerModule {

    @Singleton
    @Provides
    @Named(CONTENT_RETROFIT)
    fun provideContentRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.CONTENT_BASE_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(buildOkHttpClient())
            .build()
    }

    @Singleton
    @Provides
    @Named(API_RETROFIT)
    fun provideApiRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_BASE_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(buildOkHttpClient())
            .build()
    }

    private fun buildOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Singleton
    @Provides
    fun provideContentRetrofitService(@Named(CONTENT_RETROFIT) retrofit: Retrofit): ContentRetrofitService {
        return retrofit.create(ContentRetrofitService::class.java)
    }

    @Singleton
    @Provides
    fun provideApiRetrofitService(@Named(API_RETROFIT) retrofit: Retrofit): ApiRetrofitService {
        return retrofit.create(ApiRetrofitService::class.java)
    }

}