package com.allrites.lib.di

import com.allrites.lib.AllRitesSdk
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [SystemDaggerModule::class,
        RetrofitDaggerModule::class]
)
interface RootObjectGraph {

    fun inject(sdk: AllRitesSdk)

}