package com.allrites.lib.di

import android.content.Context
import android.content.res.Resources
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Provides various system resources (context-dependent).
 */

@Module
class SystemDaggerModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return context
    }

    @Provides
    @Singleton
    fun provideResources(): Resources {
        return context.resources
    }

}