package com.allrites.lib

import com.allrites.lib.api.ApiManager
import com.allrites.lib.core.VideoDataHolder
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class InitialRequestsSender @Inject constructor() {

    fun sendInitialRequests(videoId: String): Completable {
        return apiManager.postApiActivity()
            .flatMap { apiActivityResponse ->
                if (apiActivityResponse.isSuccessful()) {
                    apiManager.getVideo(videoId)
                } else {
                    Single.error(Throwable(apiActivityResponse.message))
                }
            }
            .flatMapCompletable { videoResponse ->
                if (videoResponse.isSuccessful()) {
                    videoDataHolder.setVideoData(videoResponse.getVideoData())
                    Completable.complete()
                } else {
                    Completable.error(Throwable(videoResponse.message))
                }
            }
    }

    //region Dependencies
    @Inject
    lateinit var apiManager: ApiManager

    @Inject
    lateinit var videoDataHolder: VideoDataHolder
    //endregion
}