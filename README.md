## About
The library sends analytics for a video playback. Uses the ExoPlayer.

## Project structure:
* `lib` folder - source code for library;
* `test_app` folder - the sample app shows how to use the aar file;
* `aar_module` folder - a module with the aar file.

## Import library
In order to import library:
- Create a new module with the AAR package using the file `lib-release-*.aar`
- Make sure the library is listed at the top of your settings.gradle file, as shown here for a library named `aar_module`:
```groovy
include ':aar_module'
```
- Open the app module's build.gradle file and add a new line to the dependencies block:
```
dependencies {
	...
    implementation project(":aar_module")
}
```
- Add the library dependencies to the app module's build.gradle file:
```
dependencies {
	...
	implementation "org.jetbrains.kotlin:kotlin-stdlib:1.4.31"
    implementation 'androidx.core:core-ktx:1.3.2'
	implementation 'com.google.dagger:dagger:2.33'
    kapt 'com.google.dagger:dagger-compiler:2.33'
    implementation 'com.google.android.exoplayer:exoplayer:2.13.1'
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.squareup.okhttp3:okhttp:4.9.0'
    implementation('com.squareup.okhttp3:logging-interceptor:4.9.0')
    implementation 'com.google.code.gson:gson:2.8.6'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
    implementation 'io.reactivex.rxjava3:rxjava:3.0.4'
    implementation 'io.reactivex.rxjava3:rxandroid:3.0.0'
    implementation 'com.squareup.retrofit2:adapter-rxjava3:2.9.0'
}
```
- Add the plugins to the app module's build.gradle file:
```
plugins {
    ...
    id 'kotlin-android'
    id 'kotlin-kapt'
}
```
- Set Java 1.8 in compileOptions in the app module's `build.gradle` file:
```
android {
	...
	compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
}
```
- Click **Sync Project with Gradle Files**.

## Usage
In order to use the library, you need to create an application-wide instance of the AllRitesSdk class:
```
val sdk = AllRitesSdk(context)
```
You need to init the library before the usage, and deinit it after the usage (it can be done once per lifetime of an application):
```
sdk.init("api_token", "client_id")
...
sdk.deinit()
```
You need to start the video session simultaneously with the player preparation, and end the video session when the player is no longer playing:
```
...
player.playWhenReady = true
player.prepare()
sdk.startVideoSession(player)
...
sdk.endVideoSession()
```

## Error handling
You can listen for any errors occurred by using an error callback:
```
sdk.setErrorCallback(errorCallback)
```

## Build
In order to build the aar library from the sources you need to download the sources and run the gradle task `Tasks/other/bundleReleaseAar` in Android Studio. You'll get the output aar file in the `lib\build\outputs\aar\` folder.