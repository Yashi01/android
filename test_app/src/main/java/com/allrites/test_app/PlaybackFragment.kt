package com.allrites.test_app

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.allrites.lib.AllRitesSdk
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import kotlinx.android.synthetic.main.playback_fragment.view.*

class PlaybackFragment : Fragment() {

    private lateinit var sdk: AllRitesSdk

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sdk = AllRitesSdk(context!!)
        sdk.init(getString(R.string.api_token), getString(R.string.client_id))
    }

    override fun onDestroy() {
        super.onDestroy()
        sdk.deinit()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.playback_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val player = SimpleExoPlayer.Builder(context!!).build()
        view.exoPlayerView.player = player
        player.playWhenReady = true

        val projectUrl = "https://allrites-caas.s3-ap-southeast-1.amazonaws.com/packages/packaged-january-2021-VDYO/88074/play.m3u8"
        val testUrl = "http://demo.unified-streaming.com/video/tears-of-steel/tears-of-steel.ism/.m3u8"
        val url = projectUrl

        val dataSourceFactory: DataSource.Factory = DefaultHttpDataSource.Factory()
        val hlsMediaSource: HlsMediaSource = HlsMediaSource.Factory(dataSourceFactory)
            .createMediaSource(MediaItem.fromUri(url))
        player.setMediaSource(hlsMediaSource)

        player.prepare()

        sdk.startVideoSession(player)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        sdk.endVideoSession()
    }

}